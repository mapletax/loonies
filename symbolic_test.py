#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2023,2024 Jérôme Carretero <cJ-mapletax@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Currency stuff

import logging
import sympy

from .symbolic import *


logger = logging.getLogger(__name__)


def test_nofloats():

	a = sympy.sympify("1.456 + a")
	assert not nofloats(a)

	a = sympy.sympify("1 + a")
	assert nofloats(a)


def test_round_away():
	a = Z("1.5")
	b = round_away(a)
	assert b == Z("2")

	a = Z("1.49")
	b = round_away(a)
	assert b == Z("1")

	a = Z("0.5")
	b = round_away(a)
	assert b == Z("1")

	a = Z("0.51")
	b = round_away(a)
	assert b == Z("1")


	a = Z("-1.5")
	b = round_away(a)
	assert b == Z("-2")

	a = Z("-1.49")
	b = round_away(a)
	assert b == Z("-1")

	a = Z("-0.5")
	b = round_away(a)
	assert b == Z("-1")

	a = Z("-0.51")
	b = round_away(a)
	assert b == Z("-1")
