#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2023,2024 Jérôme Carretero <cJ-mapletax@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Numbers

import logging
import decimal

import sympy


logger = logging.getLogger(__name__)


def nofloats(expr: sympy.Expr):
	"""
	:return: True if no floats in expression
	"""
	return not any(isinstance(_, sympy.core.numbers.Float) for _ in expr.atoms())


def Z(x):
	"""
	Conversion to rational
	"""
	if isinstance(x, int):
		return sympy.Rational(x, 1)

	if isinstance(x, str):
		return sympy.Rational(*decimal.Decimal(x).as_integer_ratio())

	if isinstance(x, decimal.Decimal):
		return sympy.Rational(*x.as_integer_ratio())

	if isinstance(x, sympy.Rational):
		return x

	if isinstance(x, sympy.Float):
		raise ValueError(f"{type(x)}/{x}")

	if isinstance(x, sympy.Expr) and nofloats(x):
		return x

	raise ValueError(f"{type(x)}/{x}")


def round_up(rat: sympy.Rational):
	return sympy.floor(rat + sympy.Rational(1, 2))


def round_down(rat: sympy.Rational):
	return sympy.ceiling(rat - sympy.Rational(1, 2))


def round_away(rat: sympy.Rational):
	"""
	Round half away from zero
	"""
	return sympy.Piecewise(
	 (round_up(rat), rat>=0),
	 (round_down(rat), True),
	)


def round_towards(rat: sympy.Rational):
	"""
	Round half towards zero
	"""
	return sympy.Piecewise(
	 (round_down(rat), rat>=0),
	 (round_up(rat), True),
	)


def round_to_even(rat: sympy.Rational):
	"""
	Round to even
	"""
	i = sympy.floor(rat)
	f = rat - i

	return sympy.Piecewise(
	 (i + 1, f > sympy.Rational(1, 2)),
	 (i + 0, f < sympy.Rational(1, 2)),
	 (i + 1, i % 2 > 0),
	 (i + 0, True),
	)
