#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2023,2024 Jérôme Carretero <cJ-mapletax@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Currency stuff

import logging
from .symbolic import *
from .currency import *


logger = logging.getLogger(__name__)


def test_centround():
	a = Z("1.5") / 100
	b = centround(a)
	assert b == Z("0.02")

	a = Z("1.49") / 100
	b = centround(a)
	assert b == Z("0.01")

	a = Z("0.5") / 100
	b = centround(a)
	assert b == Z("0.00")

	a = Z("0.51") / 100
	b = centround(a)
	assert b == Z("0.01")


	a = Z("-1.5") / 100
	b = centround(a)
	assert b == Z("-0.02")

	a = Z("-1.49") / 100
	b = centround(a)
	assert b == Z("-0.01")

	a = Z("-0.5") / 100
	b = centround(a)
	assert b == Z("-0.00")

	a = Z("-0.51") / 100
	b = centround(a)
	assert b == Z("-0.01")

	try:
		b = centround(sympy.pi)
	except TypeError:
		pass


