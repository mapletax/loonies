#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2023,2024 Jérôme Carretero <cJ-mapletax@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Currency stuff

import logging
import decimal

import sympy

from .symbolic import round_to_even


logger = logging.getLogger(__name__)


class centround(sympy.Function):
	"""
	Round to (even) cents
	"""
	@classmethod
	def eval(cls, x):
		if isinstance(x, sympy.Rational):
			return round_to_even(x * 100) / 100

		if x.is_rational is False:
			raise TypeError("x should be a rational")


def R2D(x):
	"""
	Rational to $¢
	"""

	if isinstance(x, sympy.Rational):
		n, d = centround(x).as_numer_denom()
		return decimal.Decimal(int(n)) / int(d)

	raise NotImplementedError(f"{type(x)}/{x}")
	logger.warning("X = %s %s", type(x), x)

	return x
